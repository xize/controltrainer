﻿
namespace ControlTrainer
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.hooklabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.manacheckbox = new ControlTrainer.controls.CustomCheckBox();
            this.ammocheckbox = new ControlTrainer.controls.CustomCheckBox();
            this.instakillcheckbox = new ControlTrainer.controls.CustomCheckBox();
            this.godmodecheckbox = new ControlTrainer.controls.CustomCheckBox();
            this.healthfunccheckbox = new ControlTrainer.controls.CustomCheckBox();
            this.moveAbleWindow1 = new ControlTrainer.controls.MoveAbleWindow();
            this.roundedObject1 = new ControlTrainer.controls.RoundedObject();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.label6 = new System.Windows.Forms.Label();
            this.moneynumberic = new System.Windows.Forms.NumericUpDown();
            this.moneygivebtn = new ControlTrainer.controls.CustomCheckBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moneynumberic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::ControlTrainer.Properties.Resources.closebtn;
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(223, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(26, 30);
            this.panel1.TabIndex = 0;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(10, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "num0 Health Function:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(23, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "num1 Godmode:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(23, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "num2 Instakill:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(155, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "num3 infinitive ammo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(155, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "num4 Infinitive mana:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.hooklabel);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(12, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(225, 29);
            this.panel2.TabIndex = 14;
            // 
            // hooklabel
            // 
            this.hooklabel.AutoSize = true;
            this.hooklabel.BackColor = System.Drawing.Color.Transparent;
            this.hooklabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hooklabel.ForeColor = System.Drawing.Color.Gray;
            this.hooklabel.Location = new System.Drawing.Point(36, 12);
            this.hooklabel.Name = "hooklabel";
            this.hooklabel.Size = new System.Drawing.Size(15, 12);
            this.hooklabel.TabIndex = 16;
            this.hooklabel.Text = "no";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(-2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(206, 24);
            this.label7.TabIndex = 15;
            this.label7.Text = "Process:  Control_DX12.exe | Control_DX11.exe \r\nHooked:";
            // 
            // manacheckbox
            // 
            this.manacheckbox.BackColor = System.Drawing.Color.Transparent;
            this.manacheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("manacheckbox.BackgroundImage")));
            this.manacheckbox.Checked = false;
            this.manacheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.manacheckbox.Enabled2 = true;
            this.manacheckbox.IsButton = false;
            this.manacheckbox.Location = new System.Drawing.Point(255, 97);
            this.manacheckbox.Name = "manacheckbox";
            this.manacheckbox.Size = new System.Drawing.Size(29, 16);
            this.manacheckbox.TabIndex = 13;
            this.manacheckbox.CustomCheckBoxChange += new System.EventHandler<ControlTrainer.controls.CustomCheckBoxEventArgs>(this.manacheckbox_CustomCheckBoxChange);
            // 
            // ammocheckbox
            // 
            this.ammocheckbox.BackColor = System.Drawing.Color.Transparent;
            this.ammocheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ammocheckbox.BackgroundImage")));
            this.ammocheckbox.Checked = false;
            this.ammocheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ammocheckbox.Enabled2 = true;
            this.ammocheckbox.IsButton = false;
            this.ammocheckbox.Location = new System.Drawing.Point(255, 77);
            this.ammocheckbox.Name = "ammocheckbox";
            this.ammocheckbox.Size = new System.Drawing.Size(29, 16);
            this.ammocheckbox.TabIndex = 12;
            this.ammocheckbox.CustomCheckBoxChange += new System.EventHandler<ControlTrainer.controls.CustomCheckBoxEventArgs>(this.ammocheckbox_CustomCheckBoxChange);
            // 
            // instakillcheckbox
            // 
            this.instakillcheckbox.BackColor = System.Drawing.Color.Transparent;
            this.instakillcheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("instakillcheckbox.BackgroundImage")));
            this.instakillcheckbox.Checked = false;
            this.instakillcheckbox.Cursor = System.Windows.Forms.Cursors.No;
            this.instakillcheckbox.Enabled2 = false;
            this.instakillcheckbox.IsButton = false;
            this.instakillcheckbox.Location = new System.Drawing.Point(102, 115);
            this.instakillcheckbox.Name = "instakillcheckbox";
            this.instakillcheckbox.Size = new System.Drawing.Size(29, 16);
            this.instakillcheckbox.TabIndex = 11;
            this.instakillcheckbox.CustomCheckBoxChange += new System.EventHandler<ControlTrainer.controls.CustomCheckBoxEventArgs>(this.instakillcheckbox_CustomCheckBoxChange);
            // 
            // godmodecheckbox
            // 
            this.godmodecheckbox.BackColor = System.Drawing.Color.Transparent;
            this.godmodecheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("godmodecheckbox.BackgroundImage")));
            this.godmodecheckbox.Checked = false;
            this.godmodecheckbox.Cursor = System.Windows.Forms.Cursors.No;
            this.godmodecheckbox.Enabled2 = false;
            this.godmodecheckbox.IsButton = false;
            this.godmodecheckbox.Location = new System.Drawing.Point(102, 96);
            this.godmodecheckbox.Name = "godmodecheckbox";
            this.godmodecheckbox.Size = new System.Drawing.Size(29, 16);
            this.godmodecheckbox.TabIndex = 10;
            this.godmodecheckbox.CustomCheckBoxChange += new System.EventHandler<ControlTrainer.controls.CustomCheckBoxEventArgs>(this.godmodecheckbox_CustomCheckBoxChange);
            // 
            // healthfunccheckbox
            // 
            this.healthfunccheckbox.BackColor = System.Drawing.Color.Transparent;
            this.healthfunccheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("healthfunccheckbox.BackgroundImage")));
            this.healthfunccheckbox.Checked = false;
            this.healthfunccheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.healthfunccheckbox.Enabled2 = true;
            this.healthfunccheckbox.IsButton = false;
            this.healthfunccheckbox.Location = new System.Drawing.Point(114, 78);
            this.healthfunccheckbox.Name = "healthfunccheckbox";
            this.healthfunccheckbox.Size = new System.Drawing.Size(29, 16);
            this.healthfunccheckbox.TabIndex = 2;
            this.healthfunccheckbox.CustomCheckBoxChange += new System.EventHandler<ControlTrainer.controls.CustomCheckBoxEventArgs>(this.customCheckBox1_CustomCheckBoxChange);
            // 
            // moveAbleWindow1
            // 
            this.moveAbleWindow1.ControlTarget = null;
            this.moveAbleWindow1.MainWindowTarget = this;
            // 
            // roundedObject1
            // 
            this.roundedObject1.Border = 0;
            this.roundedObject1.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject1.Radius = 8;
            this.roundedObject1.TargetControl = this;
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.worker_DoWork);
            this.worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.worker_RunWorkerCompleted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gray;
            this.label6.Location = new System.Drawing.Point(155, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "num5 give Money:";
            // 
            // moneynumberic
            // 
            this.moneynumberic.BackColor = System.Drawing.Color.Black;
            this.moneynumberic.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.moneynumberic.ForeColor = System.Drawing.Color.Gray;
            this.moneynumberic.Location = new System.Drawing.Point(157, 131);
            this.moneynumberic.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.moneynumberic.Name = "moneynumberic";
            this.moneynumberic.Size = new System.Drawing.Size(92, 16);
            this.moneynumberic.TabIndex = 16;
            this.moneynumberic.Value = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            // 
            // moneygivebtn
            // 
            this.moneygivebtn.BackColor = System.Drawing.Color.Transparent;
            this.moneygivebtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moneygivebtn.BackgroundImage")));
            this.moneygivebtn.Checked = false;
            this.moneygivebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.moneygivebtn.Enabled2 = true;
            this.moneygivebtn.IsButton = true;
            this.moneygivebtn.Location = new System.Drawing.Point(255, 131);
            this.moneygivebtn.Name = "moneygivebtn";
            this.moneygivebtn.Size = new System.Drawing.Size(29, 16);
            this.moneygivebtn.TabIndex = 17;
            this.moneygivebtn.CustomCheckBoxChange += new System.EventHandler<ControlTrainer.controls.CustomCheckBoxEventArgs>(this.moneygivebtn_CustomCheckBoxChange);
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ControlTrainer.Properties.Resources.teamrevive;
            this.ClientSize = new System.Drawing.Size(484, 200);
            this.Controls.Add(this.moneygivebtn);
            this.Controls.Add(this.moneynumberic);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.manacheckbox);
            this.Controls.Add(this.ammocheckbox);
            this.Controls.Add(this.instakillcheckbox);
            this.Controls.Add(this.godmodecheckbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.healthfunccheckbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control Trainer DX12  [Steam]";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            this.Load += new System.EventHandler(this.Window_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moneynumberic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private controls.CustomCheckBox healthfunccheckbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private controls.CustomCheckBox godmodecheckbox;
        private controls.CustomCheckBox instakillcheckbox;
        private controls.CustomCheckBox ammocheckbox;
        private controls.CustomCheckBox manacheckbox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private controls.MoveAbleWindow moveAbleWindow1;
        private controls.RoundedObject roundedObject1;
        private System.Windows.Forms.Label hooklabel;
        private System.ComponentModel.BackgroundWorker worker;
        private controls.CustomCheckBox moneygivebtn;
        private System.Windows.Forms.NumericUpDown moneynumberic;
        private System.Windows.Forms.Label label6;
    }
}

