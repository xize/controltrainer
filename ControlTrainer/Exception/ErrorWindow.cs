﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlTrainer.Exception
{
    public partial class ErrorWindow : Form
    {

        [DllImport("shell32.dll", EntryPoint = "ExtractIcon")]
        public extern static IntPtr ExtractIcon(IntPtr hInst, string lpszExeFileName, int nIconIndex);


        private String path = @"C:\WINDOWS\System32\shell32.dll";

        public ErrorWindow()
        {
            InitializeComponent();
        }

        public ErrorWindow(String content)
        {
                this.InitializeComponent();
                this.TextArea = content;
            this.FormClosing += new FormClosingEventHandler(closeform);
        }

        public ErrorWindow(String content, String title)
        {
                this.InitializeComponent();
                this.TextArea = content;
                this.Title = title;
            this.FormClosing += new FormClosingEventHandler(closeform);
        }

        private void closeform(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }

        [Description("the title of the window"), Category("Appearance")]
        public String Title
        {
            get
            {
                return this.Text;
            } set
            {
                this.Text = value;
            }
        }

        [Description("the text to be edited for the stacktrace"), Category("Appearance")]
        public String TextArea
        {
            get
            {
                return this.richTextBox1.Text;
            }
            set
            {
                this.richTextBox1.Text = value;
            }
        }

        private Icon GetIcon(int index)
        {
            IntPtr Hicon = ExtractIcon(
               IntPtr.Zero, path, index);
            Icon icon = Icon.FromHandle(Hicon);
            return icon;
        }

        private void ErrorWindow_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(onKeypress);
            this.errorpanel.BackgroundImage = GetIcon(77).ToBitmap();
            //this.Icon = GetIcon(109);

            string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string path = appdata + Path.PathSeparator + @"team revive"+ Path.PathSeparator + "Borderlands3";
            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if(File.Exists(path+Path.PathSeparator+"crash.log"))
            {
                DateTime t = DateTime.Now;
                string date = t.ToString("h:m:s DD-MM-YY");

                StringBuilder build = new StringBuilder();

                build.AppendLine();
                build.AppendLine();
                build.Append("Date: "+date);
                build.AppendLine();
                build.Append("Cheat message:\n"+ richTextBox1.Text);

                File.AppendAllText(path + Path.PathSeparator + "crash.log", build.ToString());

            } else
            {
                DateTime t = DateTime.Now;
                string date = t.ToString("h:m:s DD-MM-YY");

                StringBuilder build = new StringBuilder();

                build.AppendLine();
                build.AppendLine();
                build.Append("Date: " + date);
                build.AppendLine();
                build.Append("Cheat message:\n" + richTextBox1.Text);

                File.WriteAllText(path + Path.PathSeparator + "crash.log", build.ToString());
            }

        }

        private void onKeypress(object sender, KeyPressEventArgs e)
        {
           if(e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
            }
        }

        private void Copybtn_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.TextArea);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
