﻿using ControlTrainer.CheatAPI;
using ControlTrainer.Cheats;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace ControlTrainer
{
    public partial class Window : Form
    {

        private Mem m;
        private bool hook = false;
        private bool firsttime = true;

        #region cheats
        private HealthFunctionCheat HealthFunction;
        private InfinitiveAmmoCheat Ammo;
        private ManaCheat mana;
        private MoneyCheat money;
        #endregion

        public Window()
        {
            InitializeComponent();
        }

        #region hotkeys
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, int vk);
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        private static int WM_HOTKEY = 0x0312;

        public void RegisterKey(Keys key)
        {
            RegisterHotKey(this.Handle, 0, (int)KeyModifier.None, key.GetHashCode());
        }

        enum KeyModifier
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            WinKey = 8
        }


        protected override void WndProc(ref Message m)
        {
           base.WndProc(ref m);

           // check if we got a hot key pressed.
           if (m.Msg == WM_HOTKEY)
                {
                // get the keys.
                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                ModifierKeys modifier = (ModifierKeys)((int)m.LParam & 0xFFFF);

                if(key == Keys.NumPad0)
                {
                        this.healthfunccheckbox.PerformClick();
                } else if(key == Keys.NumPad1)
                {
                        this.godmodecheckbox.PerformClick();
                } else if(key == Keys.NumPad2)
                {
                        this.instakillcheckbox.PerformClick();
                } else if(key == Keys.NumPad3)
                {
                        this.ammocheckbox.PerformClick();
                } else if(key == Keys.NumPad4)
                {
                  this.manacheckbox.PerformClick();
                } else if(key == Keys.NumPad5)
                {
                    this.moneygivebtn.PerformClick();
                }
            }
         }
         #endregion

        private void Window_Load(object sender, EventArgs e)
        {
            RegisterKey(Keys.NumPad0);
            RegisterKey(Keys.NumPad1);
            RegisterKey(Keys.NumPad2);
            RegisterKey(Keys.NumPad3);
            RegisterKey(Keys.NumPad4);
            RegisterKey(Keys.NumPad5);

            this.worker.RunWorkerAsync();
        }

        public bool HookStatus
        {
            get
            {
                return this.hook;
            }
            set
            {
                if(value)
                {
                    hooklabel.Text = "hooked";
                } else
                {
                    hooklabel.Text = "no";
                }
                this.hooklabel.Update();
                this.hook = value;
            }
        }

        private bool directx11 = false;
        private bool directx12 = false;

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if(this.m == null)
            {
                this.m = new Mem();
            }

            string labeltext = "Process: {0}\nHooked: ";

            while(true)
            {
                int pid = 0;

                if(!directx11 || !directx12) //first check if we find a directx11 process or a directx12 version
                {
                    pid = this.m.GetProcIdFromName("Control_DX11");
                    if(pid != 0)
                    {
                        this.directx11 = true;
                        this.directx12 = false;
                        this.Invoke(new MethodInvoker(delegate
                        {
                            this.label7.Text = String.Format(labeltext, "Control_DX11.exe");
                            this.label7.Update();
                        }));
                    } else
                    {
                        pid = this.m.GetProcIdFromName("Control_DX12");
                        if(pid != 0)
                        {
                            this.directx11 = false;
                            this.directx12 = true;
                            this.Invoke(new MethodInvoker(delegate
                            {
                                this.label7.Text = String.Format(labeltext, "Control_DX12.exe");
                                this.label7.Update();
                            }));
                        }
                    }
                }

                if(pid != 0)
                {
                    bool open = this.m.OpenProcess(pid);
                    if(open)
                    {
                        this.Invoke(new MethodInvoker(delegate {
                            this.HookStatus = true;
                            if(this.firsttime)
                            {
                                Console.Beep(900, 400);
                                Console.Beep(800, 400);
                                Console.Beep(900, 300);
                                Console.Beep(900, 300);
                                this.firsttime = !this.firsttime;
                            }
                        }));
                    } else
                    {
                        this.Invoke(new MethodInvoker(delegate {
                            this.HookStatus = false;
                        }));
                    }
                } else
                {
                    this.Invoke(new MethodInvoker(delegate {
                        this.HookStatus = false;
                    }));
                }
                Task t = Task.Delay(100);
                t.Wait();
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void panel1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void customCheckBox1_CustomCheckBoxChange(object sender, controls.CustomCheckBoxEventArgs e)
        {

            if(e.GetChangedStatus == controls.CheckBoxType.CHECKED)
            {
                if(!this.HookStatus)
                {
                    e.Cancel = true;
                    return;
                }

                if(this.HealthFunction == null)
                {
                    this.HealthFunction = new HealthFunctionCheat(this.m, healthfunccheckbox);
                }

                bool b = this.HealthFunction.Enable();

                if (b)
                {
                    this.godmodecheckbox.Enabled2 = true;
                    this.instakillcheckbox.Enabled2 = true;
                }
                else
                {
                    e.Cancel = true;
                }
            } else
            {
                this.HealthFunction.Disable();
                this.godmodecheckbox.Checked = false;
                this.instakillcheckbox.Checked = false;
                this.godmodecheckbox.Enabled2 = false;
                this.instakillcheckbox.Enabled2 = false;
                
            }
        }

        private void godmodecheckbox_CustomCheckBoxChange(object sender, controls.CustomCheckBoxEventArgs e)
        {
            if(!this.healthfunccheckbox.Enabled2)
            {
                return;
            }
            this.HealthFunction.ToggleGodmode();
        }

        private void instakillcheckbox_CustomCheckBoxChange(object sender, controls.CustomCheckBoxEventArgs e)
        {
            if (!this.healthfunccheckbox.Enabled2)
            {
                return;
            }
            this.HealthFunction.ToggleInstakill();
        }

        private void ammocheckbox_CustomCheckBoxChange(object sender, controls.CustomCheckBoxEventArgs e)
        {
            if(e.GetChangedStatus == controls.CheckBoxType.CHECKED)
            {
                if (!this.HookStatus)
                {
                    e.Cancel = true;
                    return;
                }

                if (this.Ammo == null)
                {
                    this.Ammo = new InfinitiveAmmoCheat(this.m, this.ammocheckbox);
                }

                this.Ammo.Enable();
            } else
            {
                this.Ammo.Disable();
            }
        }

        private void manacheckbox_CustomCheckBoxChange(object sender, controls.CustomCheckBoxEventArgs e)
        {
            if(e.GetChangedStatus == controls.CheckBoxType.CHECKED)
            {
                if(!this.HookStatus)
                {
                    e.Cancel = true;
                    return;
                }

                if(this.mana == null)
                {
                    this.mana = new ManaCheat(this.m, this.manacheckbox);
                }

                this.mana.Enable();
            } else
            {
                this.mana.Disable();
            }
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            CheatAPI.CheatRegistry.DisableAll(this, true);
            e.Cancel = false;
        }

        private void moneygivebtn_CustomCheckBoxChange(object sender, controls.CustomCheckBoxEventArgs e)
        {
            if(e.IsButtonClick)
            {

                if (!this.HookStatus)
                {
                    e.Cancel = true;
                    return;
                }

                if (this.money == null)
                {
                    this.money = new MoneyCheat(this.m, this.moneygivebtn);
                }

                if(!this.money.IsEnabled)
                {
                    bool b = this.money.Enable();
                    if(!b)
                    {
                        return;
                    }
                }

                this.money.GiveMoney((int)this.moneynumberic.Value);

            }
        }
    }
}
