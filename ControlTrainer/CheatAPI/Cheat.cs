﻿using Memory;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.CodeDom;
using System.Text;
using System.Globalization;
using System.Windows.Forms;
using ControlTrainer.controls;

namespace ControlTrainer.CheatAPI
{
    public class Cheat
    {

        private string address;
        private CustomCheckBox cs;
        private string cheatname;

        public Cheat(Mem m, string cheatname, CustomCheckBox c)
        {
            this.cheatname = cheatname;
            this.cs = c;
            GetMemory = m;
            CheatRegistry.Add(this);
        }

        /**
         * <summary>the name of the cheat being used, this shows in errors etcetera</summary>
         * <returns>CheatName - the name of the cheat</returns>
         */
        public string CheatName
        {
            get { return this.cheatname; }
        }

        /**
         * <summary>returns the memory api from Memory.dll</summary>
         * <returns>Mem - the memory.dll class</returns>
         */
        public Mem GetMemory
        {
            get; set;
        }


        /**
         * <summary>returns true if the cheat uses a checkbox, otherwise false</summary>
         * <returns>bool</returns>
         */
        public bool HasCheckbox
        {
            get
            {
                if(CustomCheckBox() != null)
                {
                    return true;
                }
                return false;
            }
        }

        /**
         * <summary>returns the CustomCheckBox Control</summary>
         * <returns>CustomCheckBox - the custom checkbox which is associated with this cheat</returns>
         */
        public CustomCheckBox CustomCheckBox()
        {
            return this.cs;
        }

        /**
         * <summary>This is the memory address returned from the AOB scan</summary>
         * <returns>string - The memory address from the AOB scan</returns>
         */
        public string Address
        {
            get
            {
                return this.address;
            }
            set
            {
                this.address = value;
            }
        }

        /**
         * <summary>this represents the orginal bytes before the instruction got replaced by a jump to our code cave</summary>
         * <returns>byte[] - the orginal instruction bytes</returns>
         */
        private byte[] RestoreInstructionJumpBytes
        {
            get; set;
        }

        /**
         * <summary>this represents the bytes we write into our code cave</summary>
         * <returns>byte[] - CodeCaveBytes</returns>
         */
        private byte[] CaveBytes
        {
            get; set;
        }

        /**
         * <summary>returns the base address of the created codecave</summary>
         * <returns>UIntPtr - the base address of the codecave</returns>
         */
        public UIntPtr AllocatedAddress
        {
            get; set;
        }

        /**
         * <summary>validates if the codecave jmp instruction has been written</summary>
         * <returns>bool - true if written, false if not written</returns>
         */
        public bool IsActive()
        {
            if (this.Address != null)
            {
                byte[] bytes = GetMemory.ReadBytes(Address, RestoreInstructionJumpBytes.Length);
                if (!bytes.SequenceEqual(RestoreInstructionJumpBytes))
                {
                    return true;
                }
            }

            return false;
        }

        /**
         * <summary>map a offset to a AOB scan for the start address</summary>
         * <returns>Cheat - cheat builder</returns>
         */
        public Cheat InjectionAOBOffset(int size)
        {

            long val = Convert.ToInt64(this.address, 16);

            val = val + size;
            this.Address = val.ToString("X");

            return this;
        }

        /**
         * <summary>sets or returns the aobsig, this is being used in errors</summary>
         * <returns>string - the aob sig</returns>
         */
        public string AOBSig
        {
            get;set;
        }

        /**
         * <summary>performs a aob scan</summary>
         * <param name="aob">aob - the aob signature</param>
         * <param name="executable">executable - if the address we search for has executable rights</param>
         * <param name="writeable">writeable - if the address we search for has write rights</param>
         * <returns>Cheat - returns the Cheat builder</returns>
         */
        public Cheat AobScan(string aob, bool writeable = false, bool executable = true)
        {
            this.AOBSig = aob;
            Task<IEnumerable<long>> t = GetMemory.AoBScan(aob, writeable, executable);
            t.Wait();
            String address = t.Result.FirstOrDefault().ToString("X");

            this.Address = address;


            return this;

        }

        /**
         * <summary>performs a aob scan</summary>
         * <param name="start">start - the offset start range</param>
         * <param name="end">end = the offset end range</param>
         * <param name="aob">aob - the aob signature</param>
         * <param name="executable">executable - if the address we search for has executable rights</param>
         * <param name="writeable">writeable - if the address we search for has write rights</param>
         * <returns>Cheat - returns the Cheat builder</returns>
         */
        public Cheat AobScan(long start, long end, string aob, bool writeable = false, bool executable = true)
        {
            this.AOBSig = aob;
            Task<IEnumerable<long>> t = GetMemory.AoBScan(start, end, aob, writeable, executable);
            t.Wait();
            
            String address = t.Result.FirstOrDefault().ToString("X");

            this.Address = address;

            return this;

        }

        /**
         * <summary>sets the orginal bytes before the address get replaced by jump</summary>
         * <param name="bytes">byte[] - the orginal bytes</param>
         * <returns>Cheat - the cheat builder</returns>
         */
        public Cheat RestoreJumpInstructionBytes(byte[] bytes)
        {
            RestoreInstructionJumpBytes = bytes;
            return this;
        }

        /**
         * <summary>adds the cavebytes</summary>
         * <returns>Cheat - the cheat builder</returns>
         */
        public Cheat AddCaveBytes(byte[] bytes)
        {
            CaveBytes = bytes;

            return this;
        }

        private int AllocSize { get; set; } = 0;

        /**
         * <summary>enforces allocation size</summary>
         * <param name="size">size - the size of the enforced allocation</param>
         * <returns>Cheat - the cheat builder</returns>
         */
        public Cheat EnforceAllocSize(int size)
        {
            AllocSize = size;
            return this;
        }

        public int GetCaveSize
        {
            get
            {
                return this.CaveBytes.Length;
            }
        }

        /**
         * <summary>
         * attempts to create the codecave<br></br><br></br>
         * throws error when the address is null or empty, the code cave was empty, or the restore bytes had 0 bytes or less than 5.
         * </summary>
         * <returns>bool - true if the execution succeeded, false when not</returns>
         */
        public bool Execute()
        {
            //first check if minimal requirements is good enough...

            if(this.Address == "0" || this.address == string.Empty)
            {
                Error.ErrorAOBNotFound(this.AOBSig, this.CheatName);
                this.ForceDisable();
                return false;
            } else if(this.CaveBytes == null || this.CaveBytes.Length <= 0)
            {
                Error.ErrorEmptyCodeCave(this.CheatName);
                this.ForceDisable();
                return false;
            } else if(this.RestoreInstructionJumpBytes == null || this.RestoreInstructionJumpBytes.Count() < 5)
            {
                Error.ErrorRestoreJMPBytesEmpty(this.CheatName);
                this.ForceDisable();
                return false;
            }


            UIntPtr ptr = UIntPtr.Zero;

            if (this.AllocSize > 0)
            {
                ptr = this.GetMemory.CreateCodeCave(Address, CaveBytes, RestoreInstructionJumpBytes.Length, this.AllocSize);
            }
            else
            {
                ptr = this.GetMemory.CreateCodeCave(Address, CaveBytes, RestoreInstructionJumpBytes.Length, 0x1000);
            }

            if (ptr != UIntPtr.Zero)
            {
                this.AllocatedAddress = ptr;

                //now check if we need to hotpatch this so it works like cheatengine.
                //we don't need to remove bytes anymore from RestoreInstructionJumpBytes because we patch our instruction and restore the full range of bytes from the insturction.
                if(this.RestoreInstructionJumpBytes.Length > 7)
                {
                    //some bytes are still remaining instead of nopping what memory.dll does, we use cheatengines way we blindly add this assuming this work with all games considering compatibility of 32bit and 64bit it may not work.
                    //first read our new instruction.
                    List<byte> bytes = this.GetMemory.ReadBytes(this.Address, 5).ToList(); //always 5.
                    
                    //now add the following bytes to finish the remaining bytes
                    List<byte> patch = new byte[] { 0x0F, 0x1F, 0x00 }.ToList(); //instead of replacing it by nops, this is what cheatengine does.

                    //append the new bytes to our jump.
                    bytes.AddRange(patch);

                    //now rewrite our jump instruction to met the cheatengine compatibility.
                    this.GetMemory.WriteBytes(this.Address, bytes.ToArray());
                } else if(this.RestoreInstructionJumpBytes.Length == 6)
                {
                    //since it is one byte more, we nop it.
                    List<byte> bytes = this.GetMemory.ReadBytes(this.Address, 5).ToList(); //always 5.

                    //now add the following byte to finish the remaining byte
                    List<byte> patch = new byte[] { 0x90 }.ToList(); //replace this with one nop as we observed from cheatengine.

                    //append the new byte to our jump.
                    bytes.AddRange(patch);

                    //now rewrite our jump instruction to met the cheatengine compatibility.
                    this.GetMemory.WriteBytes(this.Address, bytes.ToArray());
                } else if(this.RestoreInstructionJumpBytes.Length == 7)
                {
                    //since it is 2 bytes more we do this.
                    List<byte> bytes = this.GetMemory.ReadBytes(this.Address, 5).ToList(); //always 5.

                    //now add the following bytes to finish the remaining byte
                    List<byte> patch = new byte[] { 0x66,0x90 }.ToList(); //replace this so we emulate cheatengine

                    //append the new byte to our jump.
                    bytes.AddRange(patch);

                    //now rewrite our jump instruction to met the cheatengine compatibility.
                    this.GetMemory.WriteBytes(this.Address, bytes.ToArray());
                }
                //end hotpatching

                return true;
            }
            else
            {
                Error.ErrorCodeCaveCreationFailed(this.CheatName);
                this.ForceDisable();
                return false;
            }
        }

        /**
         * <summary>forcily disable control</summary>
         */
        public void ForceDisable()
        {
            if (this.HasCheckbox)
            {
                if (this.CustomCheckBox().Checked)
                {
                    this.CustomCheckBox().PerformClick();
                }
            }
        }

        /**
         * <summary>returns the parent form of the user control in this cheat</summary>
         * <returns>Form - the winform of where the user control(s) in this cheat belong to</returns>
         */
        public Form GetParentForm()
            {
                if (this.HasCheckbox)
                {
                    return this.CustomCheckBox().ParentForm;
                }
            return null;
        }

        /**
         * <summary>restores the orginal address</summary>
         * <returns>bool - true when restored, false when not hooked</returns>
         */
        public bool Restore()
        {

            Form f = this.GetParentForm();
            if(f is Window)
            {
                Window win = (Window)f;
                if(!win.HookStatus)
                {
                    //don't restore anything
                    return false;
                }
            }

            byte[] restorejmp = RestoreInstructionJumpBytes;

            Memory.Mem.VirtualFreeEx(this.GetMemory.pHandle, this.AllocatedAddress, (UIntPtr)0, 0x8000);
            this.GetMemory.WriteBytes(this.Address, restorejmp);

            return true;

        }

        /**
         * <summary>returns a 32/64bit compatible address</summary>
         * <returns>string</returns>
         */
        public string GetSafeAddressFromUintPtr(UIntPtr ptr)
        {
            return (this.GetMemory.Is64Bit ? ptr.ToUInt64().ToString("X") : ptr.ToUInt32().ToString("X"));
        }

    }
}
