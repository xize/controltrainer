﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTrainer.CheatAPI
{
    interface CheatFramework
    {

        bool Enable();

        void Disable();

    }

}
