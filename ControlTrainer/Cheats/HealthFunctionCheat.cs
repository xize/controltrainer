﻿using ControlTrainer.CheatAPI;
using ControlTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTrainer.Cheats
{
    public class HealthFunctionCheat : CheatAPI.CheatFramework
    {

        private Mem m;
        private CustomCheckBox cs;
        private Cheat HealthCheat;

        public HealthFunctionCheat(Mem m, CustomCheckBox cs)
        {
            this.m = m;
            this.cs = cs;
            this.HealthCheat = new Cheat(this.m, "health function", this.cs);
        }

        private UIntPtr GodmodeFlag
        {
            get;set;
        }

        private UIntPtr InstaKillFlag
        {
            get;set;
        }

        public void ToggleGodmode()
        {
            int i = this.m.ReadInt(GodmodeFlag.ToUInt64().ToString("X"));
            if(i == 1)
            {
                this.m.WriteMemory(GodmodeFlag.ToUInt64().ToString("X"), "int", 0 + "");
            } else
            {
                this.m.WriteMemory(GodmodeFlag.ToUInt64().ToString("X"), "int", 1 + "");
            }
        }

        public void ToggleInstakill()
        {
            int i = this.m.ReadInt(InstaKillFlag.ToUInt64().ToString("X"));
            if (i == 1)
            {
                this.m.WriteMemory(InstaKillFlag.ToUInt64().ToString("X"), "int", 0 + "");
            }
            else
            {
                this.m.WriteMemory(InstaKillFlag.ToUInt64().ToString("X"), "int", 1 + "");
            }
        }

        public bool Enable()
        {
            this.HealthCheat.AobScan("F3 0F 5D F3 F3 0F 11 73 64").
            InjectionAOBOffset(4).
            AddCaveBytes(new byte[] {
                0x66,0x9C,0x81,0xBB,0xA8,
                0x00,0x00,0x00,0x01,0x00,
                0x00,0x00,0x0F,0x84,0x16,
                0x00,0x00,0x00,0x81,0xBB,
                0xA8,0x00,0x00,0x00,0x00,
                0x00,0x00,0x00,0x0F,0x84,
                0x32,0x00,0x00,0x00,0x0F,
                0x85,0x57,0x00,0x00,0x00,
                0x81,0x3D,0x59,0x00,0x00,
                0x00,0x01,0x00,0x00,0x00,
                0x0F,0x85,0x47,0x00,0x00,
                0x00,0x81,0xBB,0xB4,0x00,
                0x00,0x00,0x00,0x00,0x00,
                0x00,0x0F,0x85,0x0C,0x00,
                0x00,0x00,0x66,0x9D,0xF3,
                0x0F,0x11,0x5B,0x64,0xE9,
                0xB9,0x28,0x33,0x00,0x81,
                0x3D,0x31,0x00,0x00,0x00,
                0x01,0x00,0x00,0x00,0x0F,
                0x85,0x1B,0x00,0x00,0x00,
                0x81,0x7B,0x64,0x0A,0xD7,
                0x23,0x3C,0x0F,0x8E,0x0E,
                0x00,0x00,0x00,0xC7,0x43,
                0x64,0x0A,0xD7,0x23,0x3C,
                0x66,0x9D,0xE9,0x8E,0x28,
                0x33,0x00,0x66,0x9D,0xF3,
                0x0F,0x11,0x73,0x64
            }).RestoreJumpInstructionBytes(new byte[] {
                0xF3,0x0F,0x11,0x73,0x64
            });

            bool b = this.HealthCheat.Execute();
            if(b)
            {
                UIntPtr baseptr = this.HealthCheat.AllocatedAddress;
                this.GodmodeFlag = UIntPtr.Add(baseptr, 0x8B);
                this.InstaKillFlag = UIntPtr.Add(GodmodeFlag, 0x4);
                return true;
            }
            return false;
        }

        public void Disable()
        {
            this.HealthCheat.Restore();
        }
    }
}
