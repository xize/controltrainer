﻿using ControlTrainer.CheatAPI;
using ControlTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlTrainer.Cheats
{
    public class MoneyCheat : CheatAPI.CheatFramework
    {
        private Mem m;
        private CustomCheckBox cs;
        private Cheat money;

        public MoneyCheat(Mem m, CustomCheckBox cs)
        {
            this.m = m;
            this.cs = cs;
            this.money = new Cheat(this.m, "money cheat", this.cs);
        }

        public bool IsEnabled
        {
            get;private set;
        }

        public UIntPtr MoneyFlag
        {
            get;private set;
        }

        public UIntPtr MoneyValue
        {
            get;private set;
        }

        public void GiveMoney(int money)
        {
            this.m.WriteMemory(MoneyValue.ToUInt64().ToString("X"), "int", money + "");
            this.m.WriteMemory(MoneyFlag.ToUInt64().ToString("X"), "int", 1 + "");
        }

        public bool Enable()
        {
            this.money.AobScan("48 8B 5F 40 40 32 ED").AddCaveBytes(new byte[]
            {
                0x66,0x9C,0x81,0x3D,0x3B,
                0x00,0x00,0x00,0x01,0x00,
                0x00,0x00,0x0F,0x84,0x0E,
                0x00,0x00,0x00,0x66,0x9D,
                0x48,0x8B,0x5F,0x40,0x40,
                0x30,0xED,0xE9,0x54,0x48,
                0x60,0x00,0x66,0x9D,0xC7,
                0x05,0x1B,0x00,0x00,0x00,
                0x00,0x00,0x00,0x00,0x41,
                0x54,0x4C,0x8B,0x25,0x16,
                0x00,0x00,0x00,0x44,0x01,
                0x67,0x40,0x41,0x5C,0x48,
                0x8B,0x5F,0x40,0x40,0x30,
                0xED
            }).RestoreJumpInstructionBytes(new byte[]
            {
                0x48,0x8B,0x5F,0x40,0x40,
                0x32,0xED
            });

            bool b = this.money.Execute();
            if (b)
            {
                this.IsEnabled = true;
                UIntPtr ptr = this.money.AllocatedAddress;
                this.MoneyFlag = UIntPtr.Add(ptr, 0x47);
                this.MoneyValue = UIntPtr.Add(this.MoneyFlag, 0x4);
            }
            return b;
        }

        public void Disable()
        {
            this.money.Restore();
        }
    }
}
