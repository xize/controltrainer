﻿using ControlTrainer.CheatAPI;
using ControlTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTrainer.Cheats
{
    public class InfinitiveAmmoCheat : CheatAPI.CheatFramework
    {
        private Mem m;
        private CustomCheckBox cs;
        private Cheat ammo;

        public InfinitiveAmmoCheat(Mem m, CustomCheckBox cs)
        {
            this.m = m;
            this.cs = cs;
            this.ammo = new Cheat(this.m, "infinitive ammo", this.cs);
        }

        public bool Enable()
        {
            this.ammo.AobScan("F3 0F 11 99 A8 01 00 00 48").
            AddCaveBytes(new byte[] {
                0xF3,0x0F,0x11,0x91,0xA8,
                0x01,0x00,0x00
             }).RestoreJumpInstructionBytes(new byte[] {
                 0xF3,0x0F,0x11,0x99,0xA8,
                 0x01,0x00,0x00
             });

            return this.ammo.Execute();
        }

        public void Disable()
        {
            this.ammo.Restore();
        }

    }
}
