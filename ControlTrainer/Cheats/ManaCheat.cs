﻿using ControlTrainer.CheatAPI;
using ControlTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTrainer.Cheats
{
    public class ManaCheat : CheatAPI.CheatFramework
    {
        private Mem m;
        private CustomCheckBox cs;
        private Cheat mana;

        public ManaCheat(Mem m, CustomCheckBox cs)
        {
            this.m = m;
            this.cs = cs;
            this.mana = new Cheat(this.m, "mana cheat", this.cs);
        }

        public bool Enable()
        {
            this.mana.AobScan("0F 28 E0 F3 0F 11 41 40").InjectionAOBOffset(3).AddCaveBytes(new byte[]
            {
                0xF3,0x0F,0x11,0x71,0x40

            }).RestoreJumpInstructionBytes(new byte[] {
                0xF3,0x0F,0x11,0x41,0x40
            });

            return this.mana.Execute();
        }

        public void Disable()
        {
            this.mana.Restore();
        }
    }
}
