﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTrainer.controls
{
    public class CustomCheckBoxEventArgs : EventArgs
    {

        private CheckBoxType before;
        private CheckBoxType now;
        private CheckBoxType button;

        public CustomCheckBoxEventArgs(CheckBoxType before, CheckBoxType now)
        {
            this.before = before;
            this.now = now;
        }

        public CustomCheckBoxEventArgs(CheckBoxType button)
        {
            this.button = button;
        }


        public bool IsButtonClick
        {
            get
            {
                if(button.Equals(CheckBoxType.CLICKED))
                    return true;

                return false;
            }
        }

        public CheckBoxType GetChangedStatus
        {
            get
            {
                return this.now;
            }
        }

        public CheckBoxType GetPreviousStatus
        {
            get
            {
                return this.before;
            }
        }

        public bool Cancel
        {
            get;set;
        }

    }

    public enum CheckBoxType
    {
        CHECKED,
        UNCHECKED,
        CLICKED
    }

}
