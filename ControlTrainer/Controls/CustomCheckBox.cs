﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlTrainer.controls
{
    public partial class CustomCheckBox : UserControl
    {
        private bool isbutton = false;
        private bool enabled2 = true;
        private bool cchecked = false;

        SpeechSynthesizer speak = new SpeechSynthesizer();

        public event EventHandler<CustomCheckBoxEventArgs> CustomCheckBoxChange;

        public CustomCheckBox()
        {
            InitializeComponent();
        }


        public bool Enabled2
        {
            get
            {
                return this.enabled2;
            }
            set
            {
                if(value)
                {
                    if(IsButton)
                    {
                       this.Cursor = Cursors.Hand;
                       this.BackgroundImage = Properties.Resources.button;
                    } else
                    {
                        this.Cursor = Cursors.Hand;
                        if (this.Checked)
                        {
                            this.BackgroundImage = Properties.Resources.buttonon;
                        } else
                        {
                            this.BackgroundImage = Properties.Resources.buttonoff;
                        }    
                    }
                } else
                {
                    this.Cursor = Cursors.No;
                    this.BackgroundImage = Properties.Resources.buttondisabled;
                }
                this.enabled2 = value;
                this.Update();
            }
        }

        public bool Checked
        {
            get
            {
                return this.cchecked;
            }
            set
            {
                if (!this.enabled2)
                {
                    return;
                }
                if (value)
                {
                    this.Cursor = Cursors.Hand;
                    this.BackgroundImage = Properties.Resources.buttonon;
                } else
                {
                    this.Cursor = Cursors.Hand;
                    this.BackgroundImage = Properties.Resources.buttonoff;
                }
                this.cchecked = value;
                this.Update();
            }
        }

        public bool IsButton
        {
            get
            {
                return this.isbutton;
            }
            set
            {
                if(!this.enabled2)
                {
                    return;
                }
                if(value)
                {
                    this.Cursor = Cursors.Hand;
                    this.BackgroundImage = Properties.Resources.button;
                    this.isbutton = true;
                } else
                {
                    if (this.Checked)
                    {
                        this.Cursor = Cursors.Hand;
                        this.BackgroundImage = Properties.Resources.buttonon;
                    }
                    else
                    {
                        this.Cursor = Cursors.Hand;
                        this.BackgroundImage = Properties.Resources.buttonoff;
                    }
                    this.isbutton = false;
                }
                this.Update();
            }
        }

        public void PerformClick()
        {
            if(this.IsButton)
            {
                HandleClickEvent();
            } else
            {
                HandleChangeEvent();
            }
        }

        public async void HandleClickEvent()
        {
            EventHandler<CustomCheckBoxEventArgs> handler = CustomCheckBoxChange;
            if(handler != null)
            {
                //turn checkbox to load state.
                this.Invoke(new MethodInvoker(delegate
                {
                    this.enabled2 = false;
                    this.Cursor = Cursors.WaitCursor;
                    this.Update();
                }));

                CustomCheckBoxEventArgs args = new CustomCheckBoxEventArgs(CheckBoxType.CLICKED);
                Task t = Task.Run(() => handler.Invoke(this, args));

                await t;

                //wait when the task has been finished then we read back the event.

                if(t.IsCompleted)
                {

                    this.Invoke(new MethodInvoker(delegate
                    {
                        this.enabled2 = true;
                        this.Cursor = Cursors.Hand;
                        this.Update();
                    }));

                    if (args.Cancel)
                    {
                        MessageBox.Show("canceled");
                        return;
                    } else
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            speak.SpeakAsyncCancelAll();
                            speak.SpeakAsync("activated");
                            Console.Beep(900, 900);
                        }));
                    }
                } else
                {
                    //disable due error !
                    this.enabled2 = false;
                }
            } else
            {
                MessageBox.Show("no listener defined for CustomCheckBox control: " + this.Name, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async void HandleChangeEvent()
        {

            CheckBoxType before;
            CheckBoxType newstate;

            EventHandler<CustomCheckBoxEventArgs> handler = CustomCheckBoxChange;

            if (handler != null)
            {

                if(this.Checked)
                {
                    before = CheckBoxType.CHECKED;
                    newstate = CheckBoxType.UNCHECKED;
                } else
                {
                    before = CheckBoxType.UNCHECKED;
                    newstate = CheckBoxType.CHECKED;
                }

                //turn checkbox to load state.
                this.Invoke(new MethodInvoker(delegate
                {
                    this.enabled2 = false;
                    this.Cursor = Cursors.WaitCursor;
                    this.Update();
                }));
                CustomCheckBoxEventArgs args = new CustomCheckBoxEventArgs(before, newstate);
                Task t = Task.Run(() => handler.Invoke(this, args));
                await t;

                //wait when the task has been finished then we read back the event.

                if (t.IsCompleted)
                {

                    this.Invoke(new MethodInvoker(delegate
                    {
                        this.enabled2 = true;
                        this.Cursor = Cursors.Hand;

                    }));

                    if (args.Cancel)
                    {
                        return;
                    }
                    else
                    {
                        if(args.GetChangedStatus == CheckBoxType.CHECKED)
                        {
                            this.Checked = true;
                            Console.Beep(900,700);
                            speak.SpeakAsyncCancelAll();
                            speak.SpeakAsync("activated");
                        } else
                        {
                            this.Checked = false;
                            Console.Beep(700,700);
                            speak.SpeakAsyncCancelAll();
                            speak.SpeakAsync("deactivated");
                        }
                    }
                }
                else
                {
                    //disable due error !
                    this.enabled2 = false;
                }
            } else
            {
                MessageBox.Show("no listener defined for CustomCheckBox control: "+this.Name, "error", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            this.Update();
        }

        private void CustomCheckBox_Click(object sender, EventArgs e)
        {
            if(!this.Enabled2)
            {
                return;
            }
            this.PerformClick();
        }
    }
}
